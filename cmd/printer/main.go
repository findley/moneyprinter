package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"moneyprinter/internal/trade"
)

var client *trade.Client

func main() {
	clientID := os.Getenv("TDAMERITRADE_CLIENT_ID")
	if clientID == "" {
		log.Fatal("TDAMERITRADE_CLIENT_ID is missing from env")
	}

	callbackHost := os.Getenv("TDAMERITRADE_CALLBACK_HOST")
	if callbackHost == "" {
		log.Fatal("TDAMERITRADE_CALLBACK_HOST is missing from env")
	}

	var err error
	store := trade.NewFileStore("oauth_state")
	client, err = trade.NewClient(clientID, fmt.Sprintf("https://%s:4444/auth-callback", callbackHost), store)
	if err != nil {
		log.Fatal("Failed to create API client", err)
	}

	if !client.HasToken() {
		url, err := client.GetAuthUrl()
		if err != nil {
			log.Fatalf("Failed to generate a random oAuth state, %v\n", err)
		}
		fmt.Printf("Visit the URL for the auth dialog: %v", url)
	} else {
		playground(client)
	}

	http.HandleFunc("/auth-callback", callback)

	err = http.ListenAndServeTLS(":4444", "server.crt", "server.key", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func playground(client *trade.Client) {
	p, err := client.GetUserPrincipal()
	if err != nil {
		log.Fatal("Failed to get user principals:", err)
	}

	log.Printf("\n\nPrincipal: %+v\n\n", p)

	stream := trade.NewStream(p)
	go stream.Open()

	stream.AddHandler(func(r *trade.StreamResponse) {
		log.Printf("Got stream response: %+v\n", r)
	})

	// log.Println("Subscribing to USD/JPY and EUR/USD")
	// stream.SubForex([]string{"USD/JPY", "EUR/USD"})
	stream.SubAccountActivity()

	account, err := client.GetAccount(p.PrimaryAccountID)
	if err != nil {
		log.Println("Failed to get primary account", err)
	} else {
		log.Printf("Account: %s\n", account)
	}

	// time.Sleep(10 * time.Second)
	// d, err := client.GetQuote("IVDG")
	// if err != nil {
	// 	log.Println("Failed to get quote for IVDG", err)
	// } else {
	// 	log.Printf("Quote for IVDG: %s\n", d)
	// }

	// d, err = client.MarketBuy(p.PrimaryAccountID, "IVDG", 1)
	// if err != nil {
	// 	log.Println("Failed place order for IVDG", err)
	// } else {
	// 	log.Printf("Order place: %s\n", d)
	// }
}

func callback(w http.ResponseWriter, req *http.Request) {
	if client.HasToken() {
		http.Error(w, "Token is already set", http.StatusBadRequest)
		return
	}

	ctx := context.Background()
	code, ok := req.URL.Query()["code"]

	if !ok || len(code) == 0 || len(code[0]) == 0 {
		http.Error(w, "Got bad code from callback", http.StatusBadRequest)
		log.Fatal("Got bad code from callback", code)
	}

	state, ok := req.URL.Query()["state"]
	if !ok || len(state) == 0 || len(state[0]) == 0 {
		http.Error(w, "Go bad state from callback", http.StatusBadRequest)
		log.Fatal("Go bad state from callback", state)
	}

	err := client.HandleAuthCallback(ctx, code[0], state[0])
	if err != nil {
		http.Error(w, "Could not authenticate", http.StatusUnauthorized)
		log.Fatalf("Failed to get token: %v\n", err)
	}

	http.Redirect(w, req, "/quote?ticker=SPY", http.StatusTemporaryRedirect)
}
