package trade

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"golang.org/x/oauth2"
)

const authURL = "https://auth.tdameritrade.com/auth"
const tokenURL = "https://api.tdameritrade.com/v1/oauth2/token"
const baseURL = "https://api.tdameritrade.com"

type Client struct {
	oauthConfig oauth2.Config
	store       Store
	authState   *authState
}

type authState struct {
	Token      *oauth2.Token `json:"token"`
	OauthState string        `json:"state"`
}

func NewClient(clientID, redirectURL string, store Store) (*Client, error) {
	client := &Client{
		oauthConfig: oauth2.Config{
			ClientID: clientID + "@AMER.OAUTHAP",
			Endpoint: oauth2.Endpoint{
				AuthURL:  authURL,
				TokenURL: fmt.Sprintf("%s/v1/oauth2/token", baseURL),
			},
			RedirectURL: redirectURL,
		},
		authState: &authState{},
		store:     store,
	}

	err := client.loadState()
	if err != nil {
		return nil, err
	}

	return client, nil
}

func (c *Client) HasToken() bool {
	return c.authState != nil && c.authState.Token != nil
}

func (c *Client) loadState() error {
	data, err := c.store.load()
	if err != nil {
		return fmt.Errorf("Failed to load auth state from store: %w", err)
	}

	if data == nil || len(data) == 0 {
		log.Println("Token store is empty")
		return nil
	}

	err = json.Unmarshal(data, c.authState)
	if err != nil {
		return fmt.Errorf("Failed to decode auth state: %w", err)
	}

	return nil
}

func (c *Client) saveState() error {
	b, err := json.Marshal(c.authState)
	if err != nil {
		return fmt.Errorf("Failed to encode authState: %w", err)
	}

	err = c.store.save(b)
	if err != nil {
		return fmt.Errorf("Failed to save authState: %w", err)
	}

	return nil
}

func (c *Client) client() *http.Client {
	return c.oauthConfig.Client(context.Background(), c.authState.Token)
}
