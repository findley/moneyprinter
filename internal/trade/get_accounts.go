package trade

import (
	"fmt"
	"io/ioutil"
)

func (c *Client) GetAccount(accountId string) ([]byte, error) {
	if !c.HasToken() {
		return nil, fmt.Errorf("Cannot make API call, because no oauth token has been set")
	}
	client := c.client()

	resp, err := client.Get(fmt.Sprintf("%s/v1/accounts/%s?fields=orders,positions", baseURL, accountId))
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(resp.Body)
}

func (c *Client) GetAccounts() ([]byte, error) {
	if !c.HasToken() {
		return nil, fmt.Errorf("Cannot make API call, because no oauth token has been set")
	}
	client := c.client()

	resp, err := client.Get(fmt.Sprintf("%s/v1/accounts", baseURL))
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(resp.Body)
}
