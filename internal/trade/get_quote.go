package trade

import (
	"fmt"
	"io/ioutil"
)

func (c *Client) GetQuote(ticker string) ([]byte, error) {
	if !c.HasToken() {
		return nil, fmt.Errorf("Cannot make API call, because no oauth token has been set")
	}

	client := c.client()

	resp, err := client.Get(fmt.Sprintf("%s/v1/marketdata/%s/quotes", baseURL, ticker))
	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(resp.Body)
}
