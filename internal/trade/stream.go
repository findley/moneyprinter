package trade

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/websocket"
)

type Stream struct {
	principal    *UserPrincipal
	url          url.URL
	conn         *websocket.Conn
	handlers     []func(r *StreamResponse)
	outgoing     chan *StreamRequest
	ready        bool
	requestQueue []*StreamRequest
}

func NewStream(principal *UserPrincipal) *Stream {
	s := &Stream{
		principal: principal,
		url: url.URL{
			Scheme: "wss",
			Host:   principal.StreamerInfo.StreamerSocketURL,
			Path:   "/ws",
		},
		handlers:     make([]func(r *StreamResponse), 0),
		outgoing:     make(chan *StreamRequest, 10),
		ready:        false,
		requestQueue: make([]*StreamRequest, 0),
	}

	return s
}

func (s *Stream) Open() {
	log.Printf("connecting to %s", s.url.String())
	var err error
	s.conn, _, err = websocket.DefaultDialer.Dial(s.url.String(), nil)
	if err != nil {
		log.Println("Failed to dial websocket: %w", err)
		return
	}

	defer s.conn.Close()

	done := make(chan bool)

	err = s.login()
	if err != nil {
		log.Println("Failed to send login message: %w", err)
	}

	go s.readLoop(done)
	s.writeLoop(done)
}

func (s *Stream) AddHandler(h func(r *StreamResponse)) {
	s.handlers = append(s.handlers, h)
}

func (s *Stream) login() error {
	credential := url.Values{}

	tokenTimestamp, err := time.Parse("2006-01-02T15:04:05+0000", s.principal.StreamerInfo.TokenTimestamp)
	if err != nil {
		return fmt.Errorf("Failed to parse stream token timestamp: %w", err)
	}
	tokenTimeUnixMs := strconv.Itoa(int(tokenTimestamp.Unix() * 1000))

	credential.Add("userid", s.principal.Accounts[0].AccountID)
	credential.Add("token", s.principal.StreamerInfo.Token)
	credential.Add("company", s.principal.Accounts[0].Company)
	credential.Add("segment", s.principal.Accounts[0].Segment)
	credential.Add("cddomain", s.principal.Accounts[0].AccountCdDomainID)
	credential.Add("usergroup", s.principal.StreamerInfo.UserGroup)
	credential.Add("accesslevel", s.principal.StreamerInfo.AccessLevel)
	credential.Add("authorized", "Y")
	credential.Add("timestamp", tokenTimeUnixMs)
	credential.Add("appid", s.principal.StreamerInfo.AppID)
	credential.Add("acl", s.principal.StreamerInfo.Acl)

	cmd := StreamCommand{
		Requestid: "1",
		Service:   "ADMIN",
		Command:   "LOGIN",
		Account:   s.principal.PrimaryAccountID,
		Source:    s.principal.StreamerInfo.AppID,
		Parameters: map[string]string{
			"credential": credential.Encode(),
			"token":      s.principal.StreamerInfo.Token,
			"version":    "1.0",
		},
	}

	req := &StreamRequest{
		Requests: []StreamCommand{cmd},
	}

	s.outgoing <- req

	return nil
}

func (s *Stream) SubAccountActivity() {
    token := s.principal.StreamerSubscriptionKeys.Keys[0].Key // TODO: Maybe unsafe array access

	cmd := StreamCommand{
		Requestid: "3",
		Service:   "ACCT_ACTIVITY",
		Command:   "SUBS",
		Account:   s.principal.PrimaryAccountID,
		Source:    s.principal.StreamerInfo.AppID,
		Parameters: map[string]string{
			"keys":   token,
			"fields": "0,1,2,3",
		},
	}

	req := &StreamRequest{
		Requests: []StreamCommand{cmd},
	}

	s.queueRequest(req)

}

func (s *Stream) SubFuturesChart() {
	cmd := StreamCommand{
		Requestid: "2",
		Service:   "CHART_FUTURES",
		Command:   "SUBS",
		Account:   s.principal.PrimaryAccountID,
		Source:    s.principal.StreamerInfo.AppID,
		Parameters: map[string]string{
			"keys":   "/ES",
			"fields": "1,3,4,6",
		},
	}

	req := &StreamRequest{
		Requests: []StreamCommand{cmd},
	}

	s.queueRequest(req)
}

func (s *Stream) SubFuturesQuote(symbols []string) {
	cmd := StreamCommand{
		Requestid: "3",
		Service:   "LEVELONE_FUTURES",
		Command:   "SUBS",
		Account:   s.principal.PrimaryAccountID,
		Source:    s.principal.StreamerInfo.AppID,
		Parameters: map[string]string{
			"keys":   strings.Join(symbols, ","),
			"fields": "0,1,2,3,4",
		},
	}

	req := &StreamRequest{
		Requests: []StreamCommand{cmd},
	}

	s.queueRequest(req)
}

func (s *Stream) SubQuote(symbols []string) {
	cmd := StreamCommand{
		Requestid: "4",
		Service:   "QUOTE",
		Command:   "SUBS",
		Account:   s.principal.PrimaryAccountID,
		Source:    s.principal.StreamerInfo.AppID,
		Parameters: map[string]string{
			"keys":   strings.Join(symbols, ","),
			"fields": "0,1,2,3,4,5,6,7,8",
		},
	}

	req := &StreamRequest{
		Requests: []StreamCommand{cmd},
	}

	s.queueRequest(req)
}

func (s *Stream) SubNYSEBook(symbols []string) {
	cmd := StreamCommand{
		Requestid: "5",
		Service:   "LISTED_BOOK",
		Command:   "SUBS",
		Account:   s.principal.PrimaryAccountID,
		Source:    s.principal.StreamerInfo.AppID,
		Parameters: map[string]string{
			"keys":   strings.Join(symbols, ","),
			"fields": "0,1,2,3",
		},
	}

	req := &StreamRequest{
		Requests: []StreamCommand{cmd},
	}

	s.queueRequest(req)
}

func (s *Stream) SubNASDAQBook(symbols []string) {
	cmd := StreamCommand{
		Requestid: "5",
		Service:   "NASDAQ_BOOK",
		Command:   "SUBS",
		Account:   s.principal.PrimaryAccountID,
		Source:    s.principal.StreamerInfo.AppID,
		Parameters: map[string]string{
			"keys":   strings.Join(symbols, ","),
			"fields": "0,1,2,3",
		},
	}

	req := &StreamRequest{
		Requests: []StreamCommand{cmd},
	}

	s.queueRequest(req)
}

func (s *Stream) SubNewsHeadline(symbols []string) {
	cmd := StreamCommand{
		Requestid: "5",
		Service:   "NEWS_HEADLINE",
		Command:   "SUBS",
		Account:   s.principal.PrimaryAccountID,
		Source:    s.principal.StreamerInfo.AppID,
		Parameters: map[string]string{
			"keys":   strings.Join(symbols, ","),
			"fields": "0,1,2,5,8",
		},
	}

	req := &StreamRequest{
		Requests: []StreamCommand{cmd},
	}

	s.queueRequest(req)
}

func (s *Stream) SubForex(symbols []string) {
	cmd := StreamCommand{
		Requestid: "0",
		Service:   "LEVELONE_FOREX",
		Command:   "SUBS",
		Account:   s.principal.PrimaryAccountID,
		Source:    s.principal.StreamerInfo.AppID,
		Parameters: map[string]string{
			"keys":   strings.Join(symbols, ","),
			"fields": "0,1,2,3",
		},
	}

	req := &StreamRequest{
		Requests: []StreamCommand{cmd},
	}

	s.queueRequest(req)
}

func (s *Stream) queueRequest(req *StreamRequest) {
	if s.ready {
		s.outgoing <- req
	} else {
		s.requestQueue = append(s.requestQueue, req)
	}
}

func (s *Stream) flushRequestQueue() {
	log.Println("Flushing request queue")
	for _, req := range s.requestQueue {
		s.outgoing <- req
	}
	s.requestQueue = make([]*StreamRequest, 0)
}

type StreamRequest struct {
	Requests []StreamCommand `json:"requests"`
}

type StreamCommand struct {
	Requestid  string            `json:"requestid"`
	Service    string            `json:"service"`
	Command    string            `json:"command"`
	Account    string            `json:"account"`
	Source     string            `json:"source"`
	Parameters map[string]string `json:"parameters"`
}

type StreamResponse struct {
	Response []StreamServiceResponse `json:"response"`
	Data     []StreamServiceData     `json:"data"`
	Notify   []StreamDatum           `json:"notify"`
}

type StreamServiceResponse struct {
	Service   string      `json:"service"`
	Command   string      `json:"command"`
	Timestamp int64       `json:"timestamp"`
	Content   StreamDatum `json:"content"`
}

type StreamServiceData struct {
	Service   string        `json:"service"`
	Command   string        `json:"command"`
	Timestamp int64         `json:"timestamp"`
	Content   []StreamDatum `json:"content"`
}

type StreamDatum map[string]interface{}

func (d StreamDatum) getFieldInt(field string) (int, error) {
	if val, ok := d[field]; ok {
		if numberVal, ok := val.(float64); ok {
			return int(numberVal), nil
		} else {
			return 0, fmt.Errorf("Value in field %s is not an integer", field)
		}
	}

	return 0, fmt.Errorf("Stream message has no data in field %s", field)
}

func (s *Stream) readLoop(done chan<- bool) {
	defer close(done)
	for {
		_, msgBytes, err := s.conn.ReadMessage()
		if err != nil {
			done <- true
			log.Println("failed to read stream message:", err)
			return
		}

		log.Printf("Raw response: %s\n", msgBytes)
		response := &StreamResponse{}

		err = json.Unmarshal(msgBytes, response)

		if err != nil {
			log.Println("Failed to decode stream message:", err)
			continue
		}

		for _, msg := range response.Response {
			if msg.Service == "ADMIN" && msg.Command == "LOGIN" {
				err = s.handleLoginMessage(msg)
				if err != nil {
					log.Println(err)
					done <- true
					return
				}
			}
		}

		for _, h := range s.handlers {
			h(response)
		}
	}
}

func (s *Stream) handleLoginMessage(msg StreamServiceResponse) error {
	if code, err := msg.Content.getFieldInt("code"); err != nil || code != 0 {
		return fmt.Errorf("Stream login failed code: %d, error: %w\n", code, err)
	}

	log.Println("Login message received")
	s.flushRequestQueue()

	return nil
}

func (s *Stream) writeLoop(done <-chan bool) {
	for {
		select {
		case <-done:
			return
		case req := <-s.outgoing:
			b, err := json.Marshal(req)

			log.Println("Sending request:", string(b))

			if err != nil {
				log.Println("Could not encode request:", err)
				continue
			}

			err = s.conn.WriteMessage(websocket.TextMessage, b)
			if err != nil {
				log.Println("write:", err)
				return
			}
		}
	}
}
