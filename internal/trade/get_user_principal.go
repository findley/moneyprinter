package trade

import (
	"encoding/json"
	"fmt"
)

func (c *Client) GetUserPrincipal() (*UserPrincipal, error) {
	if !c.HasToken() {
		return nil, fmt.Errorf("Cannot make API call, because no oauth token has been set")
	}

	client := c.client()

	resp, err := client.Get(fmt.Sprintf("%s/v1/userprincipals?fields=streamerSubscriptionKeys,streamerConnectionInfo", baseURL))
	if err != nil {
		return nil, err
	}

	principal := &UserPrincipal{}
	dec := json.NewDecoder(resp.Body)
	err = dec.Decode(principal)

	if err != nil {
		return nil, err
	}

	return principal, nil
}

type UserPrincipal struct {
	UserID                   string                   `json:"userId"`
	UserCdDomainID           string                   `json:"userCdDomainId"`
	PrimaryAccountID         string                   `json:"primaryAccountId"`
	LastLoginTime            string                   `json:"lastLoginTime"`
	TokenExpirationTime      string                   `json:"tokenExpirationTime"`
	LoginTime                string                   `json:"loginTime"`
	AccessLevel              string                   `json:"accessLevel"`
	StalePassword            bool                     `json:"stalePassword"`
	StreamerInfo             StreamerInfo             `json:"streamerInfo"`
	StreamerSubscriptionKeys StreamerSubscriptionKeys `json:"streamerSubscriptionKeys"`
	ProfessionalStatus       string                   `json:"professionalStatus"`
	Quotes                   struct {
		IsNyseDelayed   bool `json:"isNyseDelayed"`
		IsNasdaqDelayed bool `json:"isNasdaqDelayed"`
		IsOpraDelayed   bool `json:"isOpraDelayed"`
		IsAmexDelayed   bool `json:"isAmexDelayed"`
		IsCmeDelayed    bool `json:"isCmeDelayed"`
		IsIceDelayed    bool `json:"isIceDelayed"`
		IsForexDelayed  bool `json:"isForexDelayed"`
	} `json:"quotes"`
	ExchangeAgreements struct {
		NASDAQEXCHANGEAGREEMENT string `json:"NASDAQ_EXCHANGE_AGREEMENT"`
		OPRAEXCHANGEAGREEMENT   string `json:"OPRA_EXCHANGE_AGREEMENT"`
		NYSEEXCHANGEAGREEMENT   string `json:"NYSE_EXCHANGE_AGREEMENT"`
	} `json:"exchangeAgreements"`
	Accounts []UserPrincipalAccount `json:"accounts"`
}

type StreamerInfo struct {
	AccessLevel       string `json:"accessLevel"`
	Acl               string `json:"acl"`
	AppID             string `json:"appId"`
	StreamerBinaryURL string `json:"streamerBinaryUrl"`
	StreamerSocketURL string `json:"streamerSocketUrl"`
	Token             string `json:"token"`
	TokenTimestamp    string `json:"tokenTimestamp"`
	UserGroup         string `json:"userGroup"`
}

type StreamerSubscriptionKeys struct {
	Keys []struct {
		Key string `json:"key"`
	} `json:"keys"`
}

type UserPrincipalAccount struct {
	AccountID         string `json:"accountId"`
	DisplayName       string `json:"displayName"`
	AccountCdDomainID string `json:"accountCdDomainId"`
	Company           string `json:"company"`
	Segment           string `json:"segment"`
	Acl               string `json:"acl"`
	Authorizations    struct {
		Apex               bool   `json:"apex"`
		LevelTwoQuotes     bool   `json:"levelTwoQuotes"`
		StockTrading       bool   `json:"stockTrading"`
		MarginTrading      bool   `json:"marginTrading"`
		StreamingNews      bool   `json:"streamingNews"`
		OptionTradingLevel string `json:"optionTradingLevel"`
		StreamerAccess     bool   `json:"streamerAccess"`
		AdvancedMargin     bool   `json:"advancedMargin"`
		ScottradeAccount   bool   `json:"scottradeAccount"`
	} `json:"authorizations"`
}
