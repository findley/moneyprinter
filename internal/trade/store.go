package trade

import (
	"io/ioutil"
	"os"
)

type Store interface {
	save(data []byte) error
	load() ([]byte, error)
}

type FileStore struct {
	file string
}

func NewFileStore(file string) *FileStore {
	return &FileStore{file: file}
}

func (s *FileStore) save(data []byte) error {
	return ioutil.WriteFile(s.file, data, 0600)
}

func (s *FileStore) load() ([]byte, error) {
	data, err := ioutil.ReadFile(s.file)

	if os.IsNotExist(err) {
		data := make([]byte, 0)
		return data, nil
	}

	return data, err
}
