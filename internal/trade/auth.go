package trade

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"

	"golang.org/x/oauth2"
)

func (c *Client) GetAuthUrl() (string, error) {
	oauthState, err := makeOauthState()
	if err != nil {
		return "", err
	}

	c.authState.OauthState = oauthState
	url := c.oauthConfig.AuthCodeURL(oauthState, oauth2.AccessTypeOffline)

	return url, nil
}

func (c *Client) HandleAuthCallback(ctx context.Context, code, state string) error {
	if state != c.authState.OauthState {
		return fmt.Errorf("invalid state. expected: '%v', got '%v'\n", c.authState.OauthState, state[0])
	}

	token, err := c.oauthConfig.Exchange(ctx, code, oauth2.AccessTypeOffline)
	if err != nil {
		return fmt.Errorf("Failed to exchange code for token: %w", err)
	}

	c.authState.Token = token
	c.authState.OauthState = ""
	c.saveState()

	return nil
}

func makeOauthState() (string, error) {
	b := make([]byte, 32)
	if _, err := rand.Read(b); err != nil {
		return "", err
	}

	state := base64.RawURLEncoding.EncodeToString(b)

	return state, nil
}
