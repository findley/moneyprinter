package trade

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Order struct {
	OrderId                 int         `json:"orderId,omitempty"`
	EnteredTime             string      `json:"enteredTime,omitempty"`
	CloseTime               string      `json:"closeTime,omitempty"`
	ReleaseTime             string      `json:"releaseTime,omitempty"`
	OrderType               string      `json:"orderType"`
	Session                 string      `json:"session"`
	Duration                string      `json:"duration"`
	Price                   string      `json:"price,omitempty"`
	StopPrice               string      `json:"stopPrice,omitempty"`
	StopPriceLinkBasis      string      `json:"stopPriceLinkBasis,omitempty"`
	StopPriceLinkType       string      `json:"stopPriceLinkType,omitempty"`
	StopPriceOffset         float64     `json:"stopPriceOffset,omitempty"`
	OrderStrategyType       string      `json:"orderStrategyType"`
	OrderLegCollection      []OrderLeg  `json:"orderLegCollection"`
	ChildOrders             []Order     `json:"childOrderStrategies,omitempty"`
	Status                  string      `json:"Status,omitempty"`
	OrderActivityCollection []Execution `json:"orderActivityCollection,omitempty"`
}

type OrderLeg struct {
	Instruction string     `json:"instruction"`
	Quantity    int        `json:"quantity"`
	Instrument  Instrument `json:"instrument"`
}

type Instrument struct {
	Symbol    string `json:"symbol"`
	AssetType string `json:"assetType"`
}

type Execution struct {
	ActivityType           string         `json:"activityType"`
	ExecutionType          string         `json:"executionType"`
	Quantity               int            `json:"quantity"`
	OrderRemainingQuantity int            `json:"orderRemainingQuantity"`
	ExecutionLegs          []ExecutionLeg `json:"executionLegs"`
}

type ExecutionLeg struct {
	LegId             int    `json:"legId"`
	Quantity          int    `json:"quantity"`
	MismarkedQuantity int    `json:"mismarkedQuantity"`
	Price             int    `json:"price"`
	Time              string `json:"time"`
}

func (c *Client) MarketBuy(accountId, symbol string, quantity int) ([]byte, error) {
	if !c.HasToken() {
		return nil, fmt.Errorf("Cannot make API call, because no oauth token has been set")
	}

	client := c.client()

	order := &Order{
		OrderType:         "MARKET",
		Session:           "NORMAL",
		Duration:          "DAY",
		OrderStrategyType: "SINGLE",
		OrderLegCollection: []OrderLeg{
			{
				Instruction: "BUY",
				Quantity:    quantity,
				Instrument: Instrument{
					Symbol:    symbol,
					AssetType: "EQUITY",
				},
			},
		},
	}

	jsonReq, err := json.Marshal(order)

	if err != nil {
		return nil, fmt.Errorf("Failed to encode request: %w", err)
	}

	resp, err := client.Post(
		fmt.Sprintf("%s/v1/accounts/%s/orders", baseURL, accountId),
		"application/json",
		bytes.NewBuffer(jsonReq),
	)

	if err != nil {
		return nil, err
	}

	return ioutil.ReadAll(resp.Body)
}
